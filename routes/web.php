<?php

Route::get('/', function () {
    $files = \File::files(public_path('docs'));

    return view('files', compact('files'));
});

Route::get('/docs/delete', function () {
    $files = \File::delete(\File::glob(public_path('docs/*.pdf')));

    return redirect('/');
});

Route::get('/sign', function () {
    // set time limit
    set_time_limit(60);

    // const values
    define('SIGNER_EMAIL', 'cornel.paduraru87@gmail.com');
    define('SIGNER_NAME', 'Cornel');
    define('CLIENT_USER_ID', '123');

    // create client
    $client = new LaravelDocusign\Client;

    // true only when host has self signed certificate
    $client->getClient()->getConfig()->setSSLVerification(false);

    $signer = $client->signer([
        'email' => SIGNER_EMAIL,
        'name' => SIGNER_NAME,
        'recipient_id' => "1",
        'routing_order' => "1",
        'client_user_id' => CLIENT_USER_ID
    ]);

    // name of files to send for signing
    // normally you would fetch it from db
    $neededFiles = [
        // 'C-E49BEEB1289_completionreceipt1.pdf' => ['page_number' => '1', 'x' => '400', 'y' => '520'],
        'C-E49BEEB1289_final_contract-171.pdf' => ['page_number' => '1', 'x' => '250', 'y' => '450'],
        'C-E49BEEB1289_final_contract-8.pdf' => ['page_number' => '1', 'x' => '450', 'y' => '750'],
        // 'C-E49BEEB1289_reservationreceipt1.pdf' => ['page_number' => '1', 'x' => '400', 'y' => '530']
    ];

    $files = collect(\File::allFiles(public_path('to-be-signed')))
        // here I'm filtering only for needed files
        ->filter(function ($file) use ($neededFiles) {
            return array_key_exists($file->getBasename(), $neededFiles);
        })
        ->map(function ($file, $index) use ($client) {
            $document = fopen($file->getPathname(), 'r');

            $doc = $client->document([
                'document_base64' => base64_encode(stream_get_contents($document)),
                'name' => $file->getBasename('.pdf'),
                'file_extension' => 'pdf',
                'document_id' => ($index + 1)
            ]);

            fclose($document);

            return $doc;
        });

    $signLabels = $files->map(function ($doc, $index) use ($client, $neededFiles) {
        $file = $neededFiles[$doc['name'] . '.' . $doc['file_extension']];

        return $client->signHere([
            'document_id' => $index + 1, 'page_number' => $file['page_number'], 'recipient_id' => '1',
            'tab_label' => 'SignHereTab', 'x_position' => $file['x'], 'y_position' => $file['y']
        ]);
    })->values()->all();

    $signer->setTabs($client->tabs([
        'sign_here_tabs' => $signLabels
    ]));

    $envelopeDefinition = $client->envelopeDefinition([
        'email_subject' => 'Please sign this document',
        'documents' => $files->values()->all(),
        'recipients' => $client->recipients(['signers' => [$signer]]),
        'status' => 'sent'
    ]);

    $envelopeSummary = $client->envelopes->createEnvelope($envelopeDefinition);

    $recipientViewRequest = $client->recipientViewRequest([
        'authentication_method' => 'None',
        'client_user_id' => CLIENT_USER_ID,
        'recipient_id' => '1',
        'user_name' => SIGNER_NAME,
        'email' => SIGNER_EMAIL,
        'return_url' => url('/return/' . $envelopeSummary['envelope_id'])
    ]);

    try {
        $results = $client->envelopes->createRecipientView($envelopeSummary['envelope_id'], $recipientViewRequest);
        return redirect($results['url']);
    } catch (Exception $e) {
        \Log::error($e->getResponseBody()->message);
    }
});

Route::get('/return/{envelopeId}', function ($envelopeId) {
    if (request('event') !== 'signing_complete') {
        return redirect('/');
    }

    $client = new LaravelDocusign\Client;
    $client->getClient()->getConfig()->setSSLVerification(false);

    $docs = $client->envelopes->listDocuments($envelopeId);

    foreach ($docs->getEnvelopeDocuments() as $document) {
        if (is_numeric($document->getDocumentId())) {
            $docStream = $client->envelopes->getDocument($document->getDocumentId(), $envelopeId);
            \File::put(public_path('docs/' . $document->getName() . '.pdf'), \File::get($docStream->getPathname()));
        }
    }

    return redirect('/');
});

Route::get('/docs/{docName}', function ($docName) {
    if (!$file = \File::exists(public_path('docs/' . $docName))) {
        abort(404);
    }

    return response()->download($file);
});
