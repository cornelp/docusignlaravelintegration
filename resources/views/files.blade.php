<!DOCTYPE html>
<html>
<head>
    <title>Sign Docs</title>
</head>
<body>
    @if (!count($files))
        <a href="/sign">Sign Documents</a><br>
    @else
        <a href="/docs/delete">Delete signed Documents</a>
    @endif

    <ul>
        @forelse ($files as $file)
            <li>
                <a href="docs/{{ $file->getFilename() }}">{{ $file->getFilename() }}</a>
            </li>
        @empty
            <span>No files</span>
        @endforelse
    </ul>
</body>
</html>

